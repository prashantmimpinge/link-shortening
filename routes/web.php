<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group( array('middleware' => 'frontend','namespace'=>'Frontend', 'middleware' => 'revalidate'), function(){
      
    Route::get( 'dashboard',        'DashboardController@index')->name('dashboard');
    Route::get( 'createlink',       'DashboardController@createLink')->name('create_link');
    Route::post('createlink',       'DashboardController@createLink')->name('create_link');
    Route::get( 'createlink2',      'DashboardController@createLinkStep2')->name('create_link2');
    Route::post('createlink2',      'DashboardController@createLinkStep2')->name('create_link2');
    Route::get( 'logout',           'HomepageController@logout')->name('logout');
    Route::get( 'delete/link/{id}', 'DashboardController@delete')->name('remove_link');
      
});

/*frontend login */
Route::get('lang/{lang}', 'LanguageController@switchLang')->name('lang.switch');
Route::get('/', 'Frontend\HomepageController@index');

Route::get('/signin', 'Frontend\HomepageController@signin');
Route::post('/signin', 'Frontend\HomepageController@dosignin');
Route::get('/signup','Frontend\HomepageController@signup');
Route::post('/signup', 'Frontend\HomepageController@doSignUp');
Route::any('/tiny/{myslug}', 'Frontend\HomepageController@openLink')->where('myslug','^([0-9A-Za-z\-]+)');
//Route::any('{myslug}/page/', array('as'=>'bar-page', 'uses'=>'Controllers\MyBar@index'))
//->where('myslug','^([0-9A-Za-z\-]+)?bar([0-9A-Za-z\-]+)?');
Route::get('{locale}','LanguageController@index');
