 <?php $__env->startSection('content'); ?>
<section class="content-sec signup">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="user_form">
                    <div>
                        <?php echo e(Form::open(array('url' => 'createlink'))); ?>

                            <div class="form-group">
                                <label>Create Shorten Url</label>
                                <input type="text" class="form-control" id="url" name="url" value="<?php echo e(URL::to('/')); ?>/tiny" readonly>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="long-url" name="long_url" placeholder="Enter url" required/>
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>