 <?php $__env->startSection('content'); ?>
<?php if(Session::has('success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo e(Session::get('success')); ?>

    </div>
<?php endif; ?>
<div class="container">
    <h2>Shorten Link Listing</h2>         
    <table class="table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Main Url</th>
                <th>Tiny Url</th>
                <th>Hits</th>
                <th>Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($user->links)): ?>
                <?php $__currentLoopData = $user->links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <tr>
                        <td><?php echo e($link->title); ?></td>
                        <td><?php echo e($link->long_url); ?></td>
                        <td><a href="<?php echo e($link->customize); ?>" target="_blank"><?php echo e($link->customize); ?></a></td>
                        <td><?php echo e($link->hits); ?></td>
                        <td><a href="<?php echo e(URL::to('/delete/')); ?>/link/<?php echo e($link->id); ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php else: ?>
                <h1>No record found</h1>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>