 <!-- Navigation -->
    <div class="PinkBG">
        <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="top-links">
                <ul class="nav navbar-nav TopMenu">
                      <?php if(Auth::check() && Auth::user()->role != 'admin'): ?> 
						<li><a href="<?php echo e(URL::to('logout')); ?>"><?php echo e(trans('language.logout')); ?></a></li>
                     <?php endif; ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </div>
    <?php if(Route::getCurrentRoute()->uri() == '/'): ?>
    <header class="header home-header">
     <?php else: ?>
    <header class="header TransparentHeaderBG">
            <?php endif; ?>
                <div class="container">
                <div class="row">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>
                        <div class="collapse navbar-collapse main-nav" id="bs-example-navbar-collapse-1">
                        <?php if(Auth::check() && Auth::user()->role != 'admin'): ?> 
                        <ul id="MainNavigation" class="nav navbar-nav">
                        <li><a class="<?php echo e(Route::getCurrentRoute()->getPath()=='dashboard' ?'active' :''); ?>" href="<?php echo e(URL::to('')); ?>/dashboard"><i class="fa fa-gg"></i>Dashboard</a></li>
                            <li><a class="<?php echo e(Route::getCurrentRoute()->getPath()=='createlink' ?'active' :''); ?>" href="<?php echo e(URL::to('')); ?>/createlink"><i class="fa fa-gg"></i>Create Shorten Link</a></li>
                        </ul>
                        <?php else: ?>
                        <ul id="MainNavigation" class="nav navbar-nav">
                           <li><a class="<?php echo e(Route::getCurrentRoute()->getPath()=='signin' ?'active' :''); ?>" href="<?php echo e(URL::to('/signin')); ?>"><i class="fa fa-key"></i><?php echo e(trans('language.login')); ?></a></li>
                            <li><a class="<?php echo e(Route::getCurrentRoute()->getPath()=='signup' ?'active' :''); ?>" href="<?php echo e(URL::to('/signup')); ?>"><i class="fa fa-user"></i><?php echo e(trans('language.register')); ?></a></li>
                        </ul>    
                       <?php endif; ?>
                        </div>
                </div>
                </div>
 
    </header>
    <input type="hidden" name="baseurl" value="<?php echo e(url('/')); ?>" id="baseurl" />
    <input type="hidden" name="imageurl" value="<?php echo e(URL::asset('images')); ?>" id="imageurl" />
    
