
<?php $__env->startSection('content'); ?>
<section class="content-sec signin">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-8 content-col">
                <div class="user_form">
                    <div>
                        <h6><?php echo e(trans('language.signin')); ?></h6>
                        <!--<p>Enter your username and password to log on:</p>-->
                        <?php if(Session::has('error')): ?>
                        <div class="alert alert-danger">
                          <?php echo e(Session::get('error')); ?>

                        </div>
                        <?php endif; ?>

                        <?php if(Session::has('success')): ?>
                        <div class="alert alert-success">
                          <?php echo e(Session::get('success')); ?>

                        </div>
                        <?php endif; ?>
                        <?php echo e(Form::open(array('url' => 'signin','class'=>'login-form'))); ?>

                        <ul>
                            <!--<li><?php echo e(Form::label('email', 'Email Address'),array('for' => 'form-username','class'=>'sr-only')); ?></li>-->
                            <li><i class="fa fa-user"></i> 
                                <?php echo e(Form::text('email', Input::old('email'), array('placeholder' =>trans('language.usernameplaceholder'),'class'=>'form-username form-control','id'=>'form-username','required'))); ?>

                                <p class="errors"><?php echo e($errors->first('email')); ?></p>
                            </li>
                            <!-- <li><?php echo e(Form::label('password', 'Password'),array('for' => 'form-username','class'=>'sr-only')); ?></li>-->
                            <li><i class="fa fa-lock"></i>
                                <?php echo e(Form::password('password',array('placeholder' => trans('language.passwordplaceholder'),'class'=>'form-password form-control','id'=>'form-password','required'))); ?>

					            <p class="errors"><?php echo e($errors->first('password')); ?></p>
                            </li>
                            
                            <li><input type="hidden" name="isEmpty" value=""> <br> <?php echo e(Form::submit(trans('language.signinbutton'), array('class'=>'loginbutton btn'))); ?> </li>
                            <li><span><?php echo e(trans('language.noaccount')); ?><a href="<?php echo e(URL::to('/signup')); ?>"> <?php echo e(trans('language.createone')); ?></a></span></li>
                        </ul> <?php echo e(Form::close()); ?> </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>