 <?php $__env->startSection('content'); ?>
<section class="content-sec signup">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 content-col">
                <div class="user_form">
                    <div>
                        <h6><?php echo e(trans('language.creataccounttext')); ?></h6> <?php if(Session::has('error')): ?>
                        <div class="alert alert-danger">
                            <h2><?php echo e(Session::get('error')); ?></h2> </div> <?php endif; ?> <?php echo e(Form::open(array('url' => 'signup','class'=>'login-form'))); ?>

                        <ul>
                            <!--<li><?php echo e(Form::label('email', 'Email Address'),array('for' => 'userEmail','class'=>'sr-only')); ?></li>-->
                            <li><i class="fa fa-user"></i> <?php echo e(Form::text('email', Input::old('email'), array('placeholder' => trans('language.usernameplaceholder'),'class'=>'form-username form-control','id'=>'form-username','required'))); ?>

                               <!--<span class="help-block">Your email address is also used to log in.</span>-->
                                <p class="errors"><?php echo e($errors->first('email')); ?></p>
                            </li>
                            <!-- <li><?php echo e(Form::label('password', 'Password'),array('for' => 'form-password','class'=>'sr-only')); ?></li>-->
                            <li><i class="fa fa-lock"></i> <?php echo e(Form::password('password',array('placeholder' => trans('language.passwordplaceholder'),'class'=>'form-password form-control','id'=>'form-password','required'))); ?>

                                <!--<span class="help-block">Choose a password for your new account.</span>-->
                                <p class="errors"><?php echo e($errors->first('password')); ?></p>
                            </li>
                            <!-- <li><?php echo e(Form::label('repeatpassword', 'Repeat Password'),array('for' => 'form-repeatpassword','class'=>'sr-only')); ?></li>-->
                            <li><i class="fa fa-lock"></i> <?php echo e(Form::password('password_confirmation',array('placeholder' => trans('language.rememberpasswordplaceholder'),'class'=>'form-repeatpassword form-control','id'=>'form-repeatpassword','required'))); ?>

                                <!--<span class="help-block">Type the password again. Passwords must match.</span>-->
                                <p class="errors"><?php echo e($errors->first('repeatpassword')); ?></p>
                            </li>
                            <li><?php echo e(Form::submit(trans('language.createmyaccountbutton'), array('class'=>'btn btn-success btn-icon'))); ?> </li>
                            <li><span><?php echo e(trans('language.alreadyhaveaccount')); ?><a href="<?php echo e(URL::to('/signin')); ?>"> <?php echo e(trans('language.signinnowtext')); ?></a></span></li>
                        </ul> <?php echo e(Form::close()); ?> </div>
                </div>
            </div>
        </div>
    </div>
</section> <?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>